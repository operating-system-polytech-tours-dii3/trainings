
#include <stdio.h>
#include <stdlib.h>

int i;
int j = 2;
extern int _end;
extern int _etext;
extern int __executable_start;
extern int _edata;
extern int __bss_start;

void function(int val) {
    int k2;
    static int m;
    printf("Value of val = %d\n", val);
    printf("Address of val = %p\n", &val);
    printf("Address of k2 = %p\n", &k2);
    printf("Address of m = %p\n", &m);
    printf("Address of function = %p\n", &function);
}

int main(int argc, char *argv[0], char *arge[0]) {
    int k1;
    function(5);
    printf("Address of main = %p\n", main);
    printf("Address of __executable_start = %p\n", &__executable_start);
    printf("Address of _etext = %p\n", &_etext);
    printf("Address of j = %p\n", &j);
    printf("Address of _edata = %p\n", &_edata);
    printf("Address of _bss_start = %p\n", &__bss_start);
    printf("Address of i = %p\n", &i);
    printf("Address of _end = %p\n", &_end);
    printf("Address of i = %p\n", &i);
    printf("Address of k1 = %p\n", &k1);
    printf("Address of argv[0] (program arguments) = %p\n", (void *) argv[0]);
    printf("Address of arge[0] (environment variables) = %p\n", (void *) arge[0]);
    printf("Address of argc = %p\n", &argc);
    printf("Address of argv = %p\n", argv);
    printf("Address of arge = %p\n", arge);

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    for (;;);
#pragma clang diagnostic pop

    return EXIT_SUCCESS;
}