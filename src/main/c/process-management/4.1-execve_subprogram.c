#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    printf("I am ran by an execve system call.\nProgram: %s\nWell done %s!\n", argv[0], argc > 1 ? argv[1] : "dear");
    return EXIT_SUCCESS;
}