#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    printf("Trying to run execve system call...");
    //remplacez ci-dessous "/home/jean" par le chemin du fichier executable.

    char **args = calloc(3, sizeof(char));
    args[0] = "This Program name";
    args[1] = "My Name";
    args[2] = NULL;

    if (execve("TP2-4.1-execve-sub-program", args, NULL) == -1) {
        perror("Error returned by execve: ");
    } else {
        printf("Error in system call");
    }
    return EXIT_SUCCESS;
}