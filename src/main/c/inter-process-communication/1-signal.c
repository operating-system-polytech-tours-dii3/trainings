#include <signal.h>
#include <stdio.h>
#include <unistd.h>

void handler(int sig) { // (2)
    printf("Received Signal: %d\n", sig);

    struct sigaction new_action = {};
    new_action.sa_handler = SIG_DFL; // (3)

    if (sigaction(SIGINT, &new_action, NULL) == -1) { // (1)
        perror("Failed to call sigaction system call in handler");
    }
}

int main() {
    struct sigaction new_action = {};
    new_action.sa_handler = handler;

    struct sigaction old_action = {};
    if (sigaction(SIGINT, &new_action, &old_action) == -1) { // (1)
        perror("Failed to call sigaction system call");
        return 0;
    }

    if (kill(getpid(), SIGINT) == -1) {
        perror("Failed to call kill");
        return 0;
    }
    if (kill(getpid(), SIGINT) == -1) {
        perror("Failed to call kill");
        return 0;
    }

#pragma ide diagnostic ignored "EndlessLoop"
    while (1);
}