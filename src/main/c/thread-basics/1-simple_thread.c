#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

static void *task_a(void *p_data) {
    puts("Hello world from task_a");
    return NULL;
}

int main(void) {
    puts("Main program init");

    pthread_t ta;
    pthread_create(&ta, NULL, task_a, NULL);
    puts("Thread started");

    sleep(5);
    puts("Main program end");
    return EXIT_SUCCESS;
}