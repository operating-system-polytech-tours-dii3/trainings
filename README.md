# Operating System Trainings

## Slides

[Training slides](https://operating-system-polytech-tours-dii3.gitlab.io/slides)

## Trainings

You will find is this repository several folders dedicated to the operating system trainings.

Here are the available training:

1. [Bash Scripting Training](https://operating-system-polytech-tours-dii3.gitlab.io/trainings/bash-scripting.html)
2. [Process Management](https://operating-system-polytech-tours-dii3.gitlab.io/trainings/process-management.html)
3. [Memory Segmentation](https://operating-system-polytech-tours-dii3.gitlab.io/trainings/memory-segmentation.html)
4. [Inter Process Communication](https://operating-system-polytech-tours-dii3.gitlab.io/trainings/inter-process-communication.html)
5. [Thread Basics](https://operating-system-polytech-tours-dii3.gitlab.io/trainings/thread-basics.html)
6. [Synchronization](https://operating-system-polytech-tours-dii3.gitlab.io/trainings/synchronization.html)
