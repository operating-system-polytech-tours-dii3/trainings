#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[]) {

    printf("[PID=%d] Hello...\n", getpid());
    const int pid = fork();
    if (pid == -1) {
        printf("[PID=%d] Error while creating child process using fork: %s", getpid(), strerror(errno));
        return EXIT_FAILURE;
    }

    if (pid > 0) {
        printf("[PID=%d][PARENT_PID=%d] Hello from parent process. Returned PID: [%d]\n", getpid(), getppid(),  pid);
        sleep(2);
        printf("[PID=%d][PARENT_PID=%d] Goodbye from parent process. Returned PID: [%d]\n", getpid(), getppid(), pid);
    }

    if (pid == 0) {
        printf("[PID=%d][PARENT_PID=%d] Hello from child process.\n", getpid(), getppid());
        printf("[PID=%d][PARENT_PID=%d] Goodbye from child process.\n", getpid(), getppid());
    }

    printf("[PID=%d] Bye\n", getpid());
    return EXIT_SUCCESS;
}
