#include <sys/types.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include "4-shared_memory.h"

int main() {
    
    int mem_ID = shmget(KEY, sizeof(shared_structure), 0666 | IPC_CREAT);
    if (mem_ID < 0) {
        perror("shmget");
        return EXIT_FAILURE;
    }

    shared_structure *shared_memory = shmat(mem_ID, NULL, 0);
    if (shared_memory == (void *) -1) {
        perror("shmat");
        return EXIT_FAILURE;
    }

    shared_memory->a = 2;
    shared_memory->b = 2.6544;

    for (int i = 0; i < 20; i++) {
        shared_memory->a = i;
        printf("Read data from shared memory: data.a = %d, data.b = %lf\n", shared_memory->a, shared_memory->b);
        sleep(1);
    }
    shmdt(shared_memory);

    return 0;
}