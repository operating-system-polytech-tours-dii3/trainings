#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

static void child_process(const int *pipe_fd) {
    char character = 0;
    char buffer[200];
    int index = 0;
    printf("Child process attempts to read from pipe...\n");
    do {
        if (read(pipe_fd[0], &character, 1) == 1) {
            buffer[index] = character;
            index++;
            printf("Read character: %c\n", character);
        }
    } while (character != 'q');
    buffer[index] = 0;
    printf("Read message: %s\n", buffer);
}

static void parent_process(const int *pipe_fd) {
    char *message = "I am your father!q";
    write(pipe_fd[1], message, strlen(message));
    printf("Parent process sent message to child process...\n");
}

int main(int argc, char *argv[]) {

    int pipe_fd[2];
    pipe(pipe_fd);

    int pid = fork();
    if (pid > 0) {
        parent_process(pipe_fd);
        while (getchar());

    } else if (pid == 0) {
        child_process(pipe_fd);
    } else {
        perror("Unable to fork process...");
    }

    return EXIT_SUCCESS;
}