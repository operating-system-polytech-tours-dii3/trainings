#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "4-shared_memory.h"

int main() {
    int mem_ID = shmget(KEY, sizeof(shared_structure), 0444);
    if (mem_ID < 0) {
        perror("shmget");
        return EXIT_FAILURE;
    }

    shared_structure *shared_memory = shmat(mem_ID, NULL, 0);
    if (shared_memory == (void *) -1) {
        perror("shmat");
        return EXIT_FAILURE;
    }

    for (int i = 0; i < 20; i++) {
        printf("Read data from shared memory: data.a = %d, data.b = %lf\n", shared_memory->a, shared_memory->b);
        sleep(1);
    }

    shmdt(shared_memory);

    return EXIT_SUCCESS;
}