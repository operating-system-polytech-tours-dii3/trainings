
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
#define NB_THREADS 5

typedef struct {
    int n;
} data;

void print_char_by_char(char *s, int n) {
    int i = 0;
    while (s[i] != '\0') {

        printf("%c", s[i]);
        fflush(stdout); // Forcing stdout to be displayed
        i++;

        // Sleep for 0.5 second
        struct timespec tim, tim2;
        tim.tv_sec  = 0;
        tim.tv_nsec = 500000000L;
        nanosleep(&tim , &tim2);
    }
    printf("%d \n", n);
    sleep(1);
}

void *mytask(void *p_data) {
    char *message = "Thread_n ";
    data *info = p_data;
    print_char_by_char(message, info->n);
    return NULL;
}

int main(void) {
    printf("main start\n");
    int i;
    pthread_t threads[NB_THREADS];
    data infos[NB_THREADS];
    for (i = 0; i < NB_THREADS; i++) {
        infos[i].n = i;
        pthread_create(&threads[i], NULL, mytask, &infos[i]);
    }

    for (i = 0; i < NB_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }
    printf("main end\n");
}