#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {

    int text_file_descriptor = open("text.txt", O_RDONLY);
    if (text_file_descriptor == -1) {
        perror("Unable to open text.txt file: ");
        return EXIT_FAILURE;
    }
    printf("File is opened\n");

    // Closing STDIN
    close(STDIN_FILENO);

    int val = dup(text_file_descriptor);
    if (val == -1) {
        fprintf(stderr, "Unable to execute file descriptor %d duplication: %s \n", val, strerror(errno));
        close(text_file_descriptor); // Don't bother of the return code, just attempt to clean.
        return EXIT_FAILURE;
    }
    printf("Duplication of STDIN is made\n");

    char tab[200];
    fgets(tab, sizeof(tab) / sizeof(char), stdin);
    printf("Read data from STDIN: %s\n", tab);

    if (close(text_file_descriptor)) {
        perror("Unable to close text.txt:") ;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

